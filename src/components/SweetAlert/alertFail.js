import Swal from "sweetalert2";

const alertFail = () => {
  Swal.fire({
    position: "center",
    icon: "error",
    title: "You have failed to hired !",
    showConfirmButton: false,
    timer: 1500,
  });
};

export { alertFail };
