import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

const alertSuccess = () => {
  // const navigate = useNavigate()
  Swal.fire({
    position: "center",
    icon: "success",
    title: "You have successfully hired !",
    showConfirmButton: false,
    timer: 1500,
  }).then(function () {
    window.location.href = "/";
  });
};

const alertDeleteSuccess = () => {
  // const navigate = useNavigate()
  Swal.fire({
    position: "center",
    icon: "success",
    title: "You have successfully deleted !",
    showConfirmButton: false,
    timer: 1500,
  });
};

const alertEditSuccess = () => {
  // const navigate = useNavigate()
  Swal.fire({
    position: "center",
    icon: "success",
    title: "You have successfully updated !",
    showConfirmButton: false,
    timer: 1500,
  }).then(function () {
    window.location.reload();
  });
};

const alertSignUpSuccess = () => {
  Swal.fire({
    position: "center",
    icon: "success",
    title: "You have successfully registered !",
    showConfirmButton: false,
    timer: 1500,
  }).then(function () {
    window.location.href = "/login";
  });
};

export {
  alertSuccess,
  alertDeleteSuccess,
  alertEditSuccess,
  alertSignUpSuccess,
};
