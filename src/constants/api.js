import axios from "axios";
import { ACCESS_TOKEN, DOMAIN, TOKEN } from "../util/settings/config";

const TokenCybersoft =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCBTw6FuZyAwNCIsIkhldEhhblN0cmluZyI6IjA1LzAzLzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY3Nzk3NDQwMDAwMCIsIm5iZiI6MTY1NDEwMjgwMCwiZXhwIjoxNjc4MTIyMDAwfQ.FunqYipkHrCbBATBzuJXyjGpZZxDekx1oY2qxW3_yfw";

const baseURL = "https://fiverrnew.cybersoft.edu.vn/api/";

let token = localStorage.getItem(TOKEN);

export const api = axios.create();

api.interceptors.request.use((config) => {
  config = {
    ...config,
    headers: {
      TokenCybersoft,
      token,
    },
    baseURL,
  };

  return config;
});
