import React, { useEffect } from "react";
import { Progress } from "antd";
import { Tabs, Form, Input, Collapse } from "antd";

import "./DetailJob.scss";
import "./OrderJob/OrderJob.scss";
import OrderJob from "./OrderJob/OrderJob";
import { useDispatch, useSelector } from "react-redux";
import { layCongViecChiTietAction } from "../../redux/actions/CongViecActions";
import { useParams } from "react-router-dom";
import Review from "./Review";
const { Panel } = Collapse;
export default function DetailJob() {
  const { userLogin } = useSelector((state) => state.AuthReducers);

  const { detailJob, jobId } = useParams();
  // console.log("jobId: ", jobId);
  // console.log("detailJob: ", detailJob);
  const { detailJobs } = useSelector((state) => state.CongViecReducers);
  // console.log("detailJobs: ", detailJobs);
  // let binhLuan = useSelector(state => state.binhLuanServices.binhLuan)

  const dispatch = useDispatch();
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  useEffect(() => {
    dispatch(layCongViecChiTietAction(jobId));
  }, []);

  return (
    <div className="DetailJob py-36">
      <div className="container">
        <div className="row">
          <div className="left col-8">
            <p>
              {detailJobs?.tenLoaiCongViec}
              {/* 123123 */}
              <i class="fa-solid fa-chevron-right"></i>
              {detailJobs?.tenNhomChiTietLoai}
              <i class="fa-solid fa-chevron-right"></i>
              {detailJobs?.tenChiTietLoai}
            </p>
            <h2>{detailJobs?.congViec?.tenCongViec}</h2>
            <div className="user flex">
              <img
                style={{ heigth: "35px", width: "35px" }}
                src={
                  detailJobs?.avatar
                    ? detailJobs?.avatar
                    : "https://picsum.photos/35/35"
                }
                alt=""
                className="rounded-full mr-2"
              />
              <span>{detailJobs?.tenNguoiTao} |</span>
              <div className="star text-warning ml-2">
                <i class="fa-solid fa-star"></i>
                <i class="fa-solid fa-star"></i>
                <i class="fa-solid fa-star"></i>
                <i class="fa-solid fa-star"></i>
                <i class="fa-solid fa-star"></i>
                <span>({detailJobs?.congViec?.danhGia})</span>
              </div>
            </div>
            <img
              src={detailJobs?.congViec?.hinhAnh}
              alt=""
              className="mt-3 w-full"
            />
          </div>
          <div className="right col-4 mt-3">
            <div className="tab">
              <Tabs size="large" defaultActiveKey="2" centered type="card">
                <Tabs.TabPane disabled tab="Basic" key="1">
                  <OrderJob />
                </Tabs.TabPane>
                <Tabs.TabPane tab="Stardard" key="2">
                  <OrderJob item={detailJobs} />
                </Tabs.TabPane>
                <Tabs.TabPane disabled tab="Premium" key="3">
                  <OrderJob />
                </Tabs.TabPane>
              </Tabs>
            </div>
          </div>
          <div className="bottom col-8">
            <div className="about">
              <h4 className="my-4">About This Gig</h4>

              <p className="text-muted h6 mb-4" style={{ lineHeight: 1.5 }}>
                {detailJobs?.congViec?.moTa}
              </p>
              <p>{detailJobs?.congViec?.moTaNgan}</p>
            </div>
            <hr />
            <div className="about-seller mt-5">
              {userLogin.user ? (
                <>
                  <h4>About the Seller</h4>
                  <div className="flex">
                    <img
                      src={
                        userLogin.user.avatar
                          ? userLogin.user.avatar
                          : "https://picsum.photos/50/50"
                      }
                      alt="..."
                      width={80}
                      height={50}
                      className="rounded-full mr-3"
                      style={{ height: "80px" }}
                    />
                    <div>
                      {/* <p>{userLogin.user.avatar}</p> */}
                      <span>{userLogin.user.name}</span>
                      <div>
                        {userLogin?.user?.skill.map((item, index) => {
                          return (
                            <span key={index} className="mr-2">
                              {item}
                            </span>
                          );
                        })}
                      </div>
                      {/* <p>{userLogin.user.skill}</p> */}
                      <div className="star text-warning">
                        <i class="fa-solid fa-star"></i>
                        <i class="fa-solid fa-star"></i>
                        <i class="fa-solid fa-star"></i>
                        <i class="fa-solid fa-star"></i>
                        <i class="fa-solid fa-star"></i>
                        (999+)
                      </div>
                      <button className="mt-3 rounded border border-gray-600 py-2 px-4">
                        Contact me
                      </button>
                    </div>
                  </div>
                </>
              ) : null}
            </div>

            <div className="FAQ mt-5">
              <h4>FAQ</h4>
              <Collapse bordered={false} className="bg-transparent">
                <Panel
                  style={{ fontSize: "20px" }}
                  header="What kind of projects we can deliver?"
                  key="1"
                >
                  <p style={{ fontSize: "15px", color: "gray" }}>
                    With this tools we can create any kind of project / images
                    like: Art concepts, Icons, vectors, board game material,
                    portraits, backgrounds, art for books, corporate
                    presentation material, collage, APP / WEB UI, Character
                    development, sculptures, vinyl toys, paintings, apparel and
                    accessories, etc
                  </p>
                </Panel>
                <Panel
                  style={{ fontSize: "20px" }}
                  header="Can I guide or influence the process?"
                  key="2"
                >
                  <p style={{ fontSize: "15px", color: "gray" }}>
                    Yes, thanks to our new real-time collaboration system you
                    will be able to guide or influence the creative process and
                    not waste time with final deliverables.
                  </p>
                </Panel>
                <Panel
                  style={{ fontSize: "20px" }}
                  header="Delivery includes generated artworks, prompts and trained models."
                  className=""
                  key="3"
                >
                  <p style={{ fontSize: "15px", color: "gray" }}>
                    Delivery includes generated artworks, prompts and trained
                    models.
                  </p>
                </Panel>
              </Collapse>
            </div>
            <Review maCongViec={jobId} />
          </div>
        </div>
      </div>
    </div>
  );
}
