import React, { useEffect } from "react";
import { Form, Input, Select, DatePicker } from "antd";

import { Link, useNavigate } from "react-router-dom";
import "../Login/Login.scss";
import "./Register.scss";
import { useFormik } from "formik";
import moment from "moment";
import FormEditUser from "../Profile/FormEditUser/FormEditUser";
import { useDispatch, useSelector } from "react-redux";
import { dangKyAction } from "../../redux/actions/AuthActions";

const { Option } = Select;
const formItemLayout = {
  labelCol: { xs: { span: 10 }, sm: { span: 9 } },
  wrapperCol: { xs: { span: 10 }, sm: { span: 8 } },
};

export default function Register() {
  const navigate = useNavigate();
  const { userRegister } = useSelector((state) => state.AuthReducers);

  const dispatch = useDispatch();
  const [form] = Form.useForm();
  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      name: "",
      email: "",
      password: "",
      phone: "",
      birthday: "",
      gender: "",
      skill: "",
      certification: "",
    },

    onSubmit: (values) => {
      // console.log('values', values)
      dispatch(dangKyAction(values));
    },
  });
  const handleChangeSkill = (skill) => {
    // let skill = value;
    formik.setFieldValue("skill", skill);
  };
  const handleChangeCertification = (certification) => {
    // let skill = value;
    formik.setFieldValue("certification", certification);
  };
  const handleChangeGender = (gender) => {
    // let skill = value;
    formik.setFieldValue("gender", gender);
  };

  const handleChangeDatePicker = (value) => {
    // console.log('datepickerchange',);
    let birthday = moment(value).format("DD/MM/YYYY");
    formik.setFieldValue("birthday", birthday);
  };
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <div className="Register selection:bg-green-500 selection:text-white pb-0 py-36">
      <div className=" bg-green-100 flex justify-center items-center">
        <div className="p-8 flex-1">
          <div className="w-1/2 register-width bg-white rounded-3xl mx-auto overflow-hidden shadow-xl">
            <div className="relative h-32 bg-green-500 rounded-bl-4xl">
              <svg
                className="absolute bottom-0"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 1440 320"
              >
                <path
                  fill="#ffffff"
                  fillOpacity={1}
                  d="M0,64L48,80C96,96,192,128,288,128C384,128,480,96,576,85.3C672,75,768,85,864,122.7C960,160,1056,224,1152,245.3C1248,267,1344,245,1392,234.7L1440,224L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"
                />
              </svg>
            </div>
            <div className="register-form px-10 pt-2 pb-8 bg-white rounded-tr-4xl">
              <h1 className="text-2xl text-center mb-5 font-semibold text-gray-900">
                Welcome back Fiverr !
              </h1>
              <>
                <Form
                  onFinish={formik.handleSubmit}
                  {...formItemLayout}
                  form={form}
                  // name='formEditUser'

                  // scrollToFirstError
                  size="large"
                  autoComplete="off"
                >
                  <Form.Item
                    name="name"
                    label="Name"
                    rules={[
                      {
                        required: true,
                        message: "Name can't be empty !",
                      },
                      { whitespace: true }, // khoảng trắng
                      { min: 3, message: "Name with 3 or more characters!" },
                    ]}
                    hasFeedback
                  >
                    <Input
                      name="name"
                      value={formik.values.name}
                      onChange={formik.handleChange}
                      placeholder="Input name"
                    />
                  </Form.Item>
                  <Form.Item
                    name="email"
                    label="Email"
                    rules={[
                      {
                        required: true,
                        message: "Email can't be empty !",
                      },
                      {
                        type: "email",
                        message: "Email invalidate !",
                      },
                    ]}
                    hasFeedback
                  >
                    <Input
                      name="email"
                      value={formik.values.email}
                      onChange={formik.handleChange}
                      placeholder="Input Email"
                    />
                  </Form.Item>
                  <Form.Item
                    name="password"
                    label="Password"
                    rules={[
                      {
                        required: true,
                        message: "Password can't be empty !",
                      },
                    ]}
                    hasFeedback
                  >
                    <Input.Password
                      name="password"
                      type="password"
                      value={formik.values.password}
                      onChange={formik.handleChange}
                      placeholder="Input password"
                    />
                  </Form.Item>
                  <Form.Item
                    name="confirm_password"
                    label="Confirm password"
                    dependencies={["password"]}
                    rules={[
                      {
                        required: true,
                        message: "Password can't be empty!",
                      },
                      ({ getFieldValue }) => ({
                        validator(_, value) {
                          if (!value || getFieldValue("password") === value) {
                            return Promise.resolve();
                          }
                          return Promise.reject(
                            "Re-entered password is incorrect!"
                          );
                        },
                      }),
                    ]}
                    hasFeedback
                  >
                    <Input.Password
                      name="confirm_password"
                      type="password"
                      value={formik.values.password}
                      onChange={formik.handleChange}
                      placeholder="Input password"
                    />
                  </Form.Item>
                  <Form.Item
                    name="phone"
                    label="Phone number"
                    rules={[
                      {
                        required: true,
                        message: "Phone can't be empty !",
                      },
                      {
                        message: "Phone number are wrong format !",
                        pattern: /(0|[1-9][0-9]*)$/,
                      },
                      { min: 10, message: "Must be 10 number !" },
                      { max: 10, message: "Can't over 10 number !" },
                    ]}
                    hasFeedback
                  >
                    <Input
                      style={{ width: "100%" }}
                      name="phone"
                      value={formik.values.phone}
                      onChange={formik.handleChange}
                      placeholder="Input phone number"
                    />
                  </Form.Item>

                  <Form.Item
                    label="Birthday"
                    name="birthday"
                    rules={[
                      {
                        required: true,
                        message: "Choose your day of birth !",
                      },
                    ]}
                    hasFeedback
                  >
                    <DatePicker
                      name="birthday"
                      format={"DD/MM/YYYY"}
                      value={formik.values.birthday}
                      onChange={handleChangeDatePicker}
                      placeholder="Input birthday"
                    />
                  </Form.Item>
                  <Form.Item
                    label="Gender"
                    name="gender"
                    rules={[
                      {
                        required: true,
                        message: "Choose your gender !",
                      },
                    ]}
                    hasFeedback
                  >
                    <Select
                      name="gender"
                      width="200px"
                      placeholder="Choose your gender"
                      value={formik.values.gender}
                      onChange={handleChangeGender}
                    >
                      <Option value={true}>Male</Option>
                      <Option value={false}>Female</Option>
                    </Select>
                  </Form.Item>
                  <Form.Item
                    label="Skill"
                    name="skill"
                    rules={[
                      {
                        required: true,
                        message: "Choose your skills !",
                      },
                    ]}
                    hasFeedback
                  >
                    <Select
                      mode="tags"
                      name="skill"
                      placeholder="Choose your skills"
                      value={formik.values.skill}
                      onChange={handleChangeSkill}
                    >
                      <Option value="Front-end">Front-end Developer</Option>
                      <Option value="Back-end">Back-end Developer</Option>
                      <Option value="Fullstack">Fullstack</Option>
                      <Option value="Data Analyst">Data Analyst</Option>
                      <Option value="Data Engineer">Data Engineer</Option>
                      <Option value="Designer"> Designer</Option>
                      <Option value="Dev Ops">DevOps</Option>
                      <Option value="UX-UI">UX/UI</Option>
                      <Option value="Other">Other</Option>
                    </Select>
                  </Form.Item>
                  <Form.Item
                    label="Certification"
                    name="certification"
                    requiredMark="optional"
                  >
                    <Select
                      showSearch
                      name="certification"
                      mode="tags"
                      placeholder="Choose your certifications"
                      value={formik.values.certification}
                      onChange={handleChangeCertification}
                    >
                      <Option value="CyberSoft Academy">
                        CyberSoft Academy
                      </Option>
                      <Option value="AWS">
                        AWS Certified Developer Associate
                      </Option>
                      <Option value="CCA">
                        CCA Spark and Hadoop Developer
                      </Option>
                      <Option value="PCAP">Python Programming</Option>
                      <Option value="Other">Other</Option>
                    </Select>
                  </Form.Item>
                  <Form.Item className="text-center">
                    <button
                      className="bg-green-500 text-gray-100 text-xl p-2 w-96 rounded-full tracking-wide
                          font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-green-600
                          shadow-lg"
                    >
                      Register
                    </button>
                  </Form.Item>
                </Form>
              </>
              <div className="text-center mt-3">
                You have already account ?{" "}
                <span className="link-primary">
                  <Link to="/login">Sign In</Link>
                </span>
              </div>
              <a
                href="#"
                className="mt-4 block text-sm text-center font-medium text-rose-600 hover:underline focus:outline-none focus:ring-2 focus:ring-rose-500"
              >
                Forgot password ?{" "}
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
